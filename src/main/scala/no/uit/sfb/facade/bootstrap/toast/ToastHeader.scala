package no.uit.sfb.facade.bootstrap.toast

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

//Tips: use <.strong()
object ToastHeader {

  class Props(val closeButton: UndefOr[Boolean] = undefined,
              val closeLabel: UndefOr[String] = undefined,
              val className: UndefOr[String] = "d-flex justify-content-between",
  ) extends js.Object

  @JSImport("react-bootstrap", "ToastHeader")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(closeButton: Boolean = true,
            closeLabel: String = "Close")(content: VdomNode*): VdomElement =
    componentJS(new Props(closeButton, closeLabel))(content: _*)
}
