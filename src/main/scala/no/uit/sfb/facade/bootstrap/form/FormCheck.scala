package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.Ref.Handle
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.raw.React.RefHandle
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._
import org.scalajs.dom.html

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}
import scala.util.Random


//If more control is needed (ex: using a non-textual label), use this as a wrapper for FormCheckLabel and FormCheckInput
//Attention: In this case all the props should be provided to FormCheckInput instead of this component (ex: checked, onChange, ...)
object FormCheck {

  class Props(
      val checked: UndefOr[Boolean] = undefined,
      val defaultChecked: UndefOr[Boolean] = undefined,
      val disabled: UndefOr[Boolean] = undefined,
      val feedbackTooltip: UndefOr[Boolean] = undefined,
      val id: UndefOr[String] = undefined,
      val inline: UndefOr[Boolean] = undefined,
      val isInvalid: UndefOr[Boolean] = undefined,
      val isValid: UndefOr[Boolean] = undefined,
      val label: UndefOr[String] = undefined,
      val onClick: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val ref: UndefOr[RefHandle[html.Input]] = undefined,
      val title: UndefOr[String] = undefined,
      val `type`: UndefOr[String] = undefined, //radio | checkbox | switch
  ) extends js.Object

  @JSImport("react-bootstrap", "FormCheck")
  @js.native
  object RawComponent extends js.Object {
    val Input: js.Object = js.native
    val Label: js.Object = js.native
  }

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.Check)

  //Controlled
  def apply(
           checked: Boolean = false,
      disabled: Boolean = false,
      feedbackTooltip: Boolean = false,
      id: String = Random.alphanumeric.take(5).mkString,
      inline: Boolean = false,
      isInvalid: Boolean = false,
      isValid: Boolean = false,
      label: String = "",
      onClick: ReactEventFromInput => Callback = _ => Callback.empty,
      onChange: ReactEventFromInput => Callback = _ => Callback.empty,
      title: String = "",
      `type`: String = "checkbox", //Radio | checkbox | switch
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        checked = checked,
        disabled = disabled,
        feedbackTooltip = feedbackTooltip,
        id = id,
        inline = inline,
        isInvalid = isInvalid,
        isValid = isValid,
        label = label,
        onClick = js.defined(onClick(_).runNow()),
        onChange = js.defined(onChange(_).runNow()),
        title = title,
        `type` = `type`
      ))(content: _*)

  //Define the ref in your Backend class as follows:
  //private val ref: Simple[Input] = Ref[html.Input]
  //Attention: do NOT use with movable items
  def uncontrolled(
             ref: Handle[html.Input],
             defaultChecked: Boolean = false,
             disabled: Boolean = false,
             feedbackTooltip: Boolean = false,
             id: UndefOr[String] = undefined, //Not needed in most cases FormGroup already sets an id automatically
             inline: Boolean = false,
             isInvalid: Boolean = false,
             isValid: Boolean = false,
             label: String = "",
             onClick: ReactEventFromInput => Callback = _ => Callback.empty,
             onChange: ReactEventFromInput => Callback = _ => Callback.empty,
             title: String = "",
             `type`: String = "checkbox", //Radio | checkbox | switch
           )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        defaultChecked = defaultChecked,
        disabled = disabled,
        feedbackTooltip = feedbackTooltip,
        id = id,
        inline = inline,
        isInvalid = isInvalid,
        isValid = isValid,
        label = label,
        onClick = js.defined(onClick(_).runNow()),
        onChange = js.defined(onChange(_).runNow()),
        ref = ref.raw,
        title = title,
        `type` = `type`
      ))(content: _*)
}
