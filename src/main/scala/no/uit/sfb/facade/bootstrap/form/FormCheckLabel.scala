package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object FormCheckLabel {

  class Props(val htmlFor: UndefOr[String] = undefined) extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](FormCheck.RawComponent.Label)

  def apply(htmlFor: UndefOr[String] = undefined,
  )(content: VdomNode*): VdomElement =
    componentJS(new Props(htmlFor = htmlFor))(content: _*)
}
