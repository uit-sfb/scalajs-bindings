package no.uit.sfb.facade.bootstrap.accordion

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Accordion {

  class Props(val activeKey: UndefOr[String] = undefined,
              val defaultActiveKey: UndefOr[String] = undefined,
              val onSelect: UndefOr[js.Function1[String, Unit]] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Accordion")
  @js.native
  object RawComponent extends js.Object {
    val Toggle: js.Object = js.native
    val Collapse: js.Object = js.native
  }

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(activeKey: UndefOr[String] = undefined,
            defaultActiveKey: UndefOr[String] = undefined,
            onSelect: String => Callback = _ => Callback.empty,
  )(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(activeKey, defaultActiveKey, js.defined(onSelect(_).runNow()))
    )(content: _*)
  }
}
