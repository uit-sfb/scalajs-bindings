package no.uit.sfb.facade.bootstrap.modal

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{
  Callback,
  Children,
  CtorType,
  JsComponent,
  ReactEventFromInput
}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object ModalDialog {

  class Props(
      val centered: UndefOr[Boolean] = undefined,
      val scrollable: UndefOr[Boolean] = undefined,
      val size: UndefOr[String] = undefined //'sm', 'lg','xl'
  ) extends js.Object

  @JSImport("react-bootstrap", "ModalDialog")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(centered: Boolean = false,
            scrollable: Boolean = false,
            size: String = "lg")(content: VdomNode*): VdomElement =
    componentJS(new Props(centered, scrollable, size))(content: _*)
}
