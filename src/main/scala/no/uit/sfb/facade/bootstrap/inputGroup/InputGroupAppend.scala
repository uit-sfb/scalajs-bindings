package no.uit.sfb.facade.bootstrap.inputGroup

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js

object InputGroupAppend {

  class Props(val controlId: String) extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](InputGroup.RawComponent.Append)

  def apply(controlId: String)(content: VdomNode*): VdomElement =
    componentJS(new Props(controlId))(content: _*)
}
