package no.uit.sfb.facade.bootstrap.toast

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation.JSImport

object ToastBody {

  class Props(val className: UndefOr[String] = undefined) extends js.Object

  @JSImport("react-bootstrap", "ToastBody")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(textWhite: Boolean = false)(content: VdomNode*): VdomNode =
    componentJS(new Props(if (textWhite) "text-white" else ""))(content: _*)
}
