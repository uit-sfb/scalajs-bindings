package no.uit.sfb.facade.bootstrap.overlay

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.raw.React
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import japgolly.scalajs.react.vdom.html_<^._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object OverlayTrigger {

  class Props(val defaultShow: UndefOr[Boolean] = undefined,
              val overlay: UndefOr[React.Element] = undefined,
              val placement: UndefOr[String] = undefined,
              val rootClose: UndefOr[Boolean] = undefined,
              val trigger: UndefOr[js.Array[String]] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "OverlayTrigger")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    defaultShow: Boolean = false,
    overlay: Option[VdomElement] = None,
    placement: String = "auto",
    rootClose: Boolean = true,
    trigger: Seq[String] = Seq("hover", "focus")
  )(content: VdomNode*): VdomElement = {
    overlay match {
      case Some(overlayComp) =>
        componentJS(
          new Props(
            defaultShow,
            overlayComp.rawElement,
            placement,
            rootClose,
            js.defined(js.Array(trigger: _*))
          )
        )(<.span((^.className := "with_overlay") +: content: _*))
      case None =>
        <.span(content: _*)
    }
  }
}
