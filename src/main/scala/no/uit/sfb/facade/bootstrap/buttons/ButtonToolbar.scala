package no.uit.sfb.facade.bootstrap.buttons

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object ButtonToolbar {

  class Props(val className: UndefOr[String] = undefined) extends js.Object

  @JSImport("react-bootstrap", "ButtonToolbar")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    className: UndefOr[String] = undefined //Ex: justify-content-between (cf https://getbootstrap.com/docs/5.0/utilities/flex/#justify-content)
  )(content: VdomNode*): VdomElement =
    componentJS(new Props(className))(content: _*)
}
