package no.uit.sfb.facade.bootstrap.carousel

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js

object CarouselItem {

  class Props() extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Carousel.RawComponent.Caption)

  def apply()(content: VdomNode*): VdomElement =
    componentJS(new Props())(content: _*)
}
