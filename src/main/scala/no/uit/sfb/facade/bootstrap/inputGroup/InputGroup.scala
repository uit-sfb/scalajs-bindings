package no.uit.sfb.facade.bootstrap.inputGroup

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object InputGroup {

  class Props(val hasValidation: UndefOr[Boolean] = undefined,
              val size: UndefOr[String] = undefined,
  ) extends js.Object

  @JSImport("react-bootstrap", "InputGroup")
  @js.native
  object RawComponent extends js.Object {
    val Append: js.Object = js.native
    val Prepend: js.Object = js.native
    val Text: js.Object = js.native
  }

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(hasValidation: Boolean = false, size: UndefOr[String] = undefined)(
    content: VdomNode*
  ): VdomElement =
    componentJS(new Props(hasValidation, size))(content: _*)
}
