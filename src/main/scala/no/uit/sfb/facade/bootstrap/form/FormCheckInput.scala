package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object FormCheckInput {

  class Props(
      val checked: UndefOr[Boolean] = undefined,
      val disabled: UndefOr[Boolean] = undefined,
      val id: UndefOr[String] = undefined,
      val isInvalid: UndefOr[Boolean] = undefined,
      val isValid: UndefOr[Boolean] = undefined,
      val onClick: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val `type`: UndefOr[String] = undefined, //radio | checkbox
  ) extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](FormCheck.RawComponent.Input)

  //Controlled
  def apply(
             checked: Boolean = false,
             disabled: Boolean = false,
      id: UndefOr[String] = undefined,
      inline: Boolean = false,
      isInvalid: Boolean = false,
      isValid: Boolean = false,
      onClick: ReactEventFromInput => Callback = _ => Callback.empty,
      onChange: ReactEventFromInput => Callback = _ => Callback.empty,
      `type`: String = "checkbox",
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        checked = checked,
        disabled = disabled,
        id = id,
        isInvalid = isInvalid,
        isValid = isValid,
        onClick = js.defined(onClick(_).runNow()),
        onChange = js.defined(onChange(_).runNow()),
        `type` = `type`
      ))(content: _*)
}
