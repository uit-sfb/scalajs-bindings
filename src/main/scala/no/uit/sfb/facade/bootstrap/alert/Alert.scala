package no.uit.sfb.facade.bootstrap.alert

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Alert {

  class Props(
    val closeLabel: UndefOr[String] = undefined, //Default: Close alert
    val dismissible: UndefOr[Boolean] = undefined,
    val onClose: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
    val transition: UndefOr[Boolean] = undefined,
    val variant: UndefOr[String] = undefined,
  ) extends js.Object

  @JSImport("react-bootstrap", "Alert")
  @js.native
  object RawComponent extends js.Object {
    val Heading: js.Object = js.native
    val Link: js.Object = js.native
  }

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    closeLabel: UndefOr[String] = undefined, //Default: Close alert
    dismissible: UndefOr[Boolean] = undefined,
    onClose: ReactEvent => Callback = _ => Callback.empty,
    transition: UndefOr[Boolean] = undefined,
    variant: UndefOr[String] = undefined
  )(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(
        closeLabel,
        dismissible,
        js.defined(onClose(_).runNow()),
        transition,
        variant
      )
    )(content: _*)
  }
}
