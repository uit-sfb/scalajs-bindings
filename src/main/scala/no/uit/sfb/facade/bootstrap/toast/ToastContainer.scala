package no.uit.sfb.facade.bootstrap.toast

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

@deprecated("Doesn't work")
object ToastContainer {

  class Props(val position: UndefOr[String] = undefined) extends js.Object

  @JSImport("react-bootstrap", "ToastContainer")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    position: UndefOr[String] = undefined
  )(content: VdomNode*): VdomElement =
    componentJS(new Props(position))(content: _*)
}
