package no.uit.sfb.facade.bootstrap.pagination

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object PageItem {

  class Props(val active: UndefOr[Boolean] = undefined,
              val disabled: UndefOr[Boolean] = undefined,
              val href: UndefOr[String] = undefined,
              val onClick: UndefOr[js.Function1[ReactEvent, Unit]] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "PageItem")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    active: Boolean = false,
    disabled: Boolean = false,
    href: UndefOr[String] = undefined,
    onClick: ReactEvent => Callback = _ => Callback.empty
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        active,
        disabled,
        href,
        onClick = js.defined(onClick(_).runNow())
      )
    )(content: _*)

  def special(
    item: String, //first/prev/next/last
    disabled: Boolean = false,
    onClick: ReactEvent => Callback = _ => Callback.empty
  ): VdomNode = {
    val cmp =
      JsComponent[PageItem.Props, Children.Varargs, Null](item match {
        case "first" =>
          Pagination.RawComponent.First
        case "prev" =>
          Pagination.RawComponent.Prev
        case "next" =>
          Pagination.RawComponent.Next
        case "last" =>
          Pagination.RawComponent.Last
        case _ => Pagination.RawComponent.Ellipsis
      })
    cmp(
      new PageItem.Props(
        disabled = disabled,
        onClick = js.defined(onClick(_).runNow())
      )
    )()
  }
}
