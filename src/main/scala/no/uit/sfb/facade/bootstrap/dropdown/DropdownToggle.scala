package no.uit.sfb.facade.bootstrap.dropdown

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined, |}
import scala.util.Random

object DropdownToggle {

  class Props(val id: UndefOr[Int | String] = undefined,
              val split: UndefOr[Boolean] = undefined,
              val variant: UndefOr[String] = undefined)
      extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Dropdown.RawComponent.Toggle)

  def apply(id: Int | String = Random.alphanumeric.take(10).mkString(""),
            split: Boolean = false,
            variant: String = "primary")(content: VdomNode*): VdomElement = {
    componentJS(new Props(id, split, variant))(content: _*)
  }
}
