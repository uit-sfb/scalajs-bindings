package no.uit.sfb.facade.bootstrap.toast

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Toast {

  class Props(val animation: UndefOr[Boolean] = undefined,
              val autohide: UndefOr[Boolean] = undefined,
              val className: UndefOr[String] = undefined,
              val delay: UndefOr[Int] = undefined,
              val onClose: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
              val show: UndefOr[Boolean] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Toast")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    show: Boolean,
    animation: Boolean = true,
    autoHide: Option[Int] = None, //in ms
    variant: String = "Light", //'Primary' | 'Secondary' | 'Success' | 'Danger' | 'Warning' | 'Info' | 'Dark' | 'Light'
    onClose: ReactEvent => Callback = _ => Callback.empty
  )(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(
        animation,
        autoHide.nonEmpty,
        s"bg-${variant.toLowerCase}",
        autoHide.getOrElse(0): Int, //Type needed for som reason
        js.defined(onClose(_).runNow()),
        show
      )
    )(content: _*)
  }
}
