package no.uit.sfb.facade.bootstrap.dropdown

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object Dropdown {

  class Props(val alignRight: UndefOr[Boolean] = undefined,
              val drop: UndefOr[String] = undefined,
              val focusFirstItemOnShow: UndefOr[Boolean | String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Dropdown")
  @js.native
  object RawComponent extends js.Object {
    val Item: js.Object = js.native
    val Toggle: js.Object = js.native
    val Menu: js.Object = js.native
    val Divider: js.Object = js.native
  }

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  //If you need as="ButtonGroup", just insert manually a ButtonGroup as child
  def apply(
    alignRight: Boolean = false,
    drop: String = "down",
    focusFirstItemOnShow: Boolean | String = false
  )(content: VdomNode*): VdomElement = {
    componentJS(new Props(alignRight, drop, focusFirstItemOnShow))(content: _*)
  }
}
