package no.uit.sfb.facade.bootstrap.navbar

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object NavbarText {

  class Props(val className: UndefOr[String] = undefined) extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Navbar.RawComponent.Text)

  def apply(className: String = null)(content: VdomNode*): VdomElement =
    componentJS(new Props(className))(content: _*)
}
