package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{
  Callback,
  Children,
  CtorType,
  JsComponent,
  ReactEventFromInput,
  ReactFormEvent,
  ReactKeyboardEvent
}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Form {

  class Props(
    val action: String,
    val id: String,
    val inline: UndefOr[Boolean] = undefined,
    val method: String,
    val onSubmit: UndefOr[js.Function1[ReactFormEvent, Unit]] = undefined,
    val onKeyDown: UndefOr[js.Function1[ReactKeyboardEvent, Unit]] = undefined,
    val validated: UndefOr[Boolean] = undefined
  ) extends js.Object

  @JSImport("react-bootstrap", "Form")
  @js.native
  object RawComponent extends js.Object {
    val Group: js.Object = js.native
    val Control: js.Object = js.native
    val Label: js.Object = js.native
    val Check: js.Object = js.native
    //val Select: js.Object = js.native
    val File: js.Object = js.native
    val Text: js.Object = js.native
  }

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    action: String = "",
    id: String = "",
    inline: Boolean = false,
    method: String = "POST", //See here: https://developer.mozilla.org/en-US/docs/Learn/Forms/Sending_and_retrieving_form_data#the_method_attribute
    onSubmit: ReactFormEvent => Callback = _ => Callback.empty,
    validated: Boolean = false,
    preventEnter: Boolean = false //Attention: if used, even pressing enter in a textarea will have no effect
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        action,
        id,
        inline,
        method,
        js.defined(onSubmit(_).runNow()),
        if (preventEnter)
          js.defined(
            e =>
              if (e.key == "Enter") e.preventDefaultCB.runNow()
              else ()
          )
        else
          js.undefined,
        validated
      )
    )(content: _*)
}
