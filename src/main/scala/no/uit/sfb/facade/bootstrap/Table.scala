package no.uit.sfb.facade.bootstrap

import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

//Facade for https://react-bootstrap.github.io/components/table/

object Table {

  class Props(val bordered: UndefOr[Boolean] = undefined,
              val borderless: UndefOr[Boolean] = undefined,
              val hover: UndefOr[Boolean] = undefined,
              val responsive: UndefOr[Boolean | String] = undefined,
              val size: UndefOr[String] = undefined,
              val striped: UndefOr[Boolean] = undefined,
              val variant: UndefOr[String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Table")
  @js.native
  object RawComponent extends js.Object

  private val componentJS =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(striped: Boolean = true,
            bordered: Boolean = true,
            hover: Boolean = false,
            responsive: Boolean | String = false,
            size: String = "sm",
            variant: String = "light",
            borderless: Boolean = false)(content: VdomNode*): VdomElement =
    componentJS(
      new Props(bordered, borderless, hover, responsive, size, striped, variant)
    )(content: _*)
}
