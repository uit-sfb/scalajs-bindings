package no.uit.sfb.facade.bootstrap

import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation._

//Facade for https://react-bootstrap.github.io/components/spinners/

object Spinner {

  class Props(val animation: UndefOr[String] = undefined,
              val variant: UndefOr[String] = undefined,
              val size: UndefOr[String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Spinner")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(animation: String = "border",
            variant: String = "dark",
            size: String = "md")(content: VdomNode*): VdomElement = {
    componentJS(new Props(animation, variant, size))(content: _*)
  }
}
