package no.uit.sfb.facade.bootstrap.modal

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object Modal {

  class Props(
      val animation: UndefOr[Boolean] = undefined,
      val autoFocus: UndefOr[Boolean] = undefined,
      val backdrop: UndefOr[Boolean | String] = undefined, //true, false, "static"
      val centered: UndefOr[Boolean] = undefined,
      val contentClassName: UndefOr[String] = undefined,
      val dialogClassName: UndefOr[String] = undefined,
      val enforceFocus: UndefOr[Boolean] = undefined,
      val keyboard: UndefOr[Boolean] = undefined,
      val onEnter: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val onEntered: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val onEntering: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val onEscapeKeyDown: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val onExit: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val onExited: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val onExiting: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val onHide: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val onShow: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
      val restoreFocus: UndefOr[Boolean] = undefined,
      val scrollable: UndefOr[Boolean] = undefined,
      val show: UndefOr[Boolean] = undefined,
      val size: UndefOr[String] = undefined, //"sm", "lg", "xl"
  ) extends js.Object

  @JSImport("react-bootstrap", "Modal")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(animation: Boolean = true,
  autoFocus: Boolean = true,
  backdrop: Boolean | String = true, //true, false, "static"
  centered: Boolean = false,
  contentClassName: UndefOr[String] = undefined,
  dialogClassName: UndefOr[String] = undefined,
  enforceFocus: Boolean = true,
  keyboard: Boolean = true,
  onEnter: ReactEvent => Callback = _ => Callback.empty,
  onEntered: ReactEvent => Callback = _ => Callback.empty,
  onEntering: ReactEvent => Callback = _ => Callback.empty,
  onEscapeKeyDown: ReactEvent => Callback = _ => Callback.empty,
  onExit: ReactEvent => Callback = _ => Callback.empty,
  onExited: ReactEvent => Callback = _ => Callback.empty,
  onExiting: ReactEvent => Callback = _ => Callback.empty,
  onHide: ReactEvent => Callback = _ => Callback.empty,
  onShow: ReactEvent => Callback = _ => Callback.empty,
  restoreFocus: Boolean = true,
  scrollable: Boolean = false,
  show: Boolean = false,
  size: String = "lg")(content: VdomNode*): VdomElement =
    componentJS(new Props(
      animation,
      autoFocus,
      backdrop,
      centered,
      contentClassName,
      dialogClassName,
      enforceFocus,
      keyboard,
      js.defined(onEnter(_).runNow()),
      js.defined(onEntered(_).runNow()),
      js.defined(onEntering(_).runNow()),
      js.defined(onEscapeKeyDown(_).runNow()),
      js.defined(onExit(_).runNow()),
      js.defined(onExited(_).runNow()),
      js.defined(onExiting(_).runNow()),
      js.defined(onHide(_).runNow()),
      js.defined(onShow(_).runNow()),
      restoreFocus,
      scrollable,
      show,
      size,
      )
    )(content: _*)
}
