package no.uit.sfb.facade.bootstrap.accordion

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object AccordionCollapse {

  class Props(val eventKey: UndefOr[String] = undefined) extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Accordion.RawComponent.Collapse)

  def apply(eventKey: String)(content: VdomNode*): VdomElement =
    componentJS(new Props(eventKey))(content: _*)
}
