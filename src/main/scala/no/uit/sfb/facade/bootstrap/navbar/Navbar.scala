package no.uit.sfb.facade.bootstrap.navbar

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object Navbar {

  class Props(val bg: UndefOr[String] = undefined,
              val collapseOnSelect: UndefOr[Boolean] = undefined,
              val expand: UndefOr[Boolean | String] = undefined,
              val fixed: UndefOr[String] = undefined,
              val onSelect: UndefOr[js.Function1[String, Unit]] = undefined,
              val sticky: UndefOr[String] = undefined,
              val variant: UndefOr[String] = undefined,
  ) extends js.Object

  @JSImport("react-bootstrap", "Navbar")
  @js.native
  object RawComponent extends js.Object {
    val Brand: js.Object = js.native
    val Toggle: js.Object = js.native
    val Collapse: js.Object = js.native
    val Text: js.Object = js.native
  }

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(bg: UndefOr[String] = undefined, //dark | light | any bg-*
            collapseOnSelect: Boolean = false,
            expand: Boolean | String = true,
            fixed: UndefOr[String] = undefined,
            onSelect: String => Callback = _ => Callback.empty,
            sticky: UndefOr[String] = undefined,
            variant: String = "light" //dark | light
  )(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(
        bg,
        collapseOnSelect,
        expand,
        fixed,
        js.defined(onSelect(_).runNow()),
        sticky,
        variant
      )
    )(content: _*)
  }
}
