package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined, |}

object FormLabel {

  class Props(val column: UndefOr[Boolean] = undefined,
              val lg: UndefOr[Int | String] = undefined,
              val md: UndefOr[Int | String] = undefined,
              val sm: UndefOr[Int | String] = undefined,
              val xl: UndefOr[Int | String] = undefined,
              val xs: UndefOr[Int | String] = undefined)
      extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.Label)

  def apply(
    column: Boolean = false,
    lg: UndefOr[Int | String] = undefined,
    md: UndefOr[Int | String] = undefined,
    sm: UndefOr[Int | String] = undefined,
    xl: UndefOr[Int | String] = undefined,
    xs: UndefOr[Int | String] = undefined
  )(content: VdomNode*): VdomElement =
    componentJS(new Props(column, lg, md, sm, xl, xs))(content: _*)
}
