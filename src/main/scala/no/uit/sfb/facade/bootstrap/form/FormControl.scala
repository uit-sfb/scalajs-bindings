package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.Ref.Handle
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.raw.React.RefHandle
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._
import org.scalajs.dom.html

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object FormControl {

  class Props(
    val as: UndefOr[String] = undefined,
    val defaultValue: UndefOr[String] = undefined,
    val disabled: UndefOr[Boolean] = undefined,
    val htmlSize: UndefOr[Int] = undefined,
    val id: UndefOr[String] = undefined,
    val isInvalid: UndefOr[Boolean] = undefined,
    val isValid: UndefOr[Boolean] = undefined,
    val max: UndefOr[String] = undefined,
    val min: UndefOr[String] = undefined,
    val onBlur: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
    val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
    val onFocus: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
    val onKeyDown: UndefOr[js.Function1[ReactKeyboardEventFromInput, Unit]] =
      undefined,
    val placeholder: UndefOr[String] = undefined,
    val plaintext: UndefOr[Boolean] = undefined,
    val readOnly: UndefOr[Boolean] = undefined,
    val size: UndefOr[String] = undefined,
    val ref: UndefOr[RefHandle[html.Input]] = undefined,
    val required: Boolean = false,
    val `type`: UndefOr[String] = undefined,
    val value: UndefOr[String] = undefined
  ) extends js.Object

  @JSImport("react-bootstrap", "FormControl")
  @js.native
  object RawComponent extends js.Object {
    val Feedback: js.Object = js.native
  }

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.Control)

  //Controlled
  def apply(
    as: String = "input", //input | textarea. Use FormSelect for select
    disabled: Boolean = false,
    htmlSize: UndefOr[Int] = undefined,
    id: UndefOr[String] = undefined, //Not needed in most cases FormGroup already sets an id automatically
    isInvalid: Boolean = false,
    isValid: Boolean = false,
    max: UndefOr[String] = undefined,
    min: UndefOr[String] = undefined,
    onBlur: ReactEventFromInput => Callback = _ => Callback.empty, //Should be used in place of onChange for uncontrolled components (cf below)
    onChange: ReactEventFromInput => Callback = _ => Callback.empty, //Note: in React, onChange == onInput (cf https://reactjs.org/docs/dom-elements.html#onchange): it triggers at each key down (after onKeyDown, i.e. the character has been written down already)
    onFocus: ReactEventFromInput => Callback = _ => Callback.empty, //Should be used in place of onChange for uncontrolled components (cf below)
    onKeyDown: ReactKeyboardEventFromInput => Callback = _ => Callback.empty, //Use ev.keyCode
    placeholder: String = "",
    plaintext: Boolean = false,
    readOnly: Boolean = false,
    size: UndefOr[String] = undefined, //sm | lg
    required: Boolean = false,
    value: String = "",
    `type`: String = ""
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        as = as,
        disabled = disabled,
        htmlSize = htmlSize,
        id = id,
        isValid = isValid,
        isInvalid = isInvalid,
        max = max,
        min = min,
        onBlur = js.defined(onBlur(_).runNow()),
        onChange = js.defined(onChange(_).runNow()),
        onFocus = js.defined(onFocus(_).runNow()),
        onKeyDown = js.defined(onKeyDown(_).runNow()),
        placeholder = placeholder,
        plaintext = plaintext,
        readOnly = readOnly,
        size = size,
        required = required,
        `type` = `type`,
        value = value
      )
    )(content: _*)

  //Define the ref in your Backend class as follows:
  //private val ref: Simple[Input] = Ref[html.Input]
  //Attention: do NOT use with movable items (use UncontrolledFormControl instead)
  def uncontrolled(
    ref: Handle[html.Input],
    as: String = "input", // input | textarea. Use FormSelect for select
    defaultValue: UndefOr[String] = undefined,
    disabled: Boolean = false,
    htmlSize: UndefOr[Int] = undefined,
    id: UndefOr[String] = undefined, //Not needed in most cases FormGroup already sets an id automatically
    isInvalid: Boolean = false,
    isValid: Boolean = false,
    max: UndefOr[String] = undefined,
    min: UndefOr[String] = undefined,
    onBlur: ReactEventFromInput => Callback = _ => Callback.empty, //Should be used in place of onChange for uncontrolled components (cf below)
    onChange: ReactEventFromInput => Callback = _ => Callback.empty, //Note: in React, onChange == onInput (cf https://reactjs.org/docs/dom-elements.html#onchange): it triggers at each key down (after onKeyDown, i.e. the character has been written down already)
    onKeyDown: ReactKeyboardEventFromInput => Callback = _ => Callback.empty, //Use ev.keyCode
    placeholder: UndefOr[String] = undefined,
    plaintext: Boolean = false,
    readOnly: Boolean = false,
    size: UndefOr[String] = undefined, //sm | lg
    required: Boolean = false,
    `type`: UndefOr[String] = undefined
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        as = as,
        defaultValue = defaultValue,
        disabled = disabled,
        htmlSize = htmlSize,
        id = id,
        isValid = isValid,
        isInvalid = isInvalid,
        max = max,
        min = min,
        onBlur = js.defined(onBlur(_).runNow()),
        onChange = js.defined(onChange(_).runNow()),
        onFocus = js.defined(onBlur(_).runNow()),
        onKeyDown = js.defined(onKeyDown(_).runNow()),
        placeholder = placeholder,
        plaintext = plaintext,
        readOnly = readOnly,
        size = size,
        ref = ref.raw,
        required = required,
        `type` = `type`
      )
    )(content: _*)
}
