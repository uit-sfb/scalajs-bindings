package no.uit.sfb.facade.bootstrap.modal

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

object ModalTitle {

  class Props() extends js.Object

  @JSImport("react-bootstrap", "ModalTitle")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply()(content: VdomNode*): VdomElement =
    componentJS(new Props())(content: _*)
}
