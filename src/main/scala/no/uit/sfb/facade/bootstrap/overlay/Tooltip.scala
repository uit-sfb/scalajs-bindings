package no.uit.sfb.facade.bootstrap.overlay

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import org.scalajs.dom.html.{Anchor, Span}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

object Tooltip {

  class Props() extends js.Object

  @JSImport("react-bootstrap", "Tooltip")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply()(content: VdomNode*): VdomElement = {
    componentJS(new Props())(content: _*)
  }

  protected val linkRegexp = "\\[(.+?)\\]\\((.+?)\\)".r

  //Understand `\n` and `[link](target)`
  def text(tooltipText: String): Option[VdomElement] = {
    if (tooltipText.isEmpty)
      None
    else {
      val children = tooltipText
        .split('\n')
        .map { str =>
          val base = linkRegexp
            .split(str)
            .map { <.span(_) }
          val links = linkRegexp
            .findAllMatchIn(str)
            .map { m =>
              <.a(^.href := m.group(2), m.group(1))
            }
            .toSeq
          <.div(
            base
              .zipAll(links, <.span(), <.span())
              .flatMap { case (a, b) => Seq(a, b) }: _*
          )
        }
      Some(componentJS(new Props())(children: _*))
    }
  }

  def text(tooltipText: Option[String]): Option[VdomElement] = {
    text(tooltipText.getOrElse(""))
  }
}
