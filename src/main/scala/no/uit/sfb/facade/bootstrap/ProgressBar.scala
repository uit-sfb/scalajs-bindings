package no.uit.sfb.facade.bootstrap

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import no.uit.sfb.facade.OptionToUndefOr._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

//Facade for https://react-bootstrap.github.io/components/spinners/

object ProgressBar {

  class Props(val animated: UndefOr[Boolean] = undefined,
              val label: UndefOr[String] = undefined,
              val now: Int = 0,
              val striped: UndefOr[Boolean] = undefined,
              val variant: UndefOr[String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "ProgressBar")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(now: Int = 0,
            animated: Boolean = false,
            striped: Boolean = false,
            variant: Option[String] = None,
            label: Option[String] = None)(content: VdomNode*): VdomElement = {
    componentJS(new Props(animated, label, now, striped, variant))(content: _*)
  }
}
