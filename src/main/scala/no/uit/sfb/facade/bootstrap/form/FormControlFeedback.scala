package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object FormControlFeedback {

  class Props(val as: UndefOr[String] = undefined,
              val tooltip: Boolean = false,
              val `type`: UndefOr[String] = undefined)
      extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](
      FormControl.RawComponent.Feedback
    )

  def apply(as: UndefOr[String] = undefined, //Ex: div
            tooltip: Boolean = false,
            `type`: String = "invalid" //valid | invalid
  )(content: VdomNode*): VdomElement =
    componentJS(new Props(as = as, tooltip = tooltip, `type` = `type`))(
      content: _*
    )
}
