package no.uit.sfb.facade.bootstrap.buttons

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent}
import no.uit.sfb.facade.OptionToUndefOr._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object ButtonGroup {

  class Props(val size: UndefOr[String] = undefined,
              val vertical: UndefOr[Boolean] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "ButtonGroup")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(size: UndefOr[String] = undefined, //sm | lg
            vertical: Boolean = false)(content: VdomNode*): VdomElement =
    componentJS(new Props(size, vertical))(content: _*)
}
