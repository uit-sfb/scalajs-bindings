package no.uit.sfb.facade.bootstrap.popover

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

object Popover {

  class Props(val id: String) extends js.Object

  @JSImport("react-bootstrap", "Popover")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(id: String)(content: VdomNode*): VdomElement = {
    componentJS(new Props(id))(content: _*)
  }
}
