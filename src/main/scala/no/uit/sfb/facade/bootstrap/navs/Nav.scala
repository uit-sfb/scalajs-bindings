package no.uit.sfb.facade.bootstrap.navs

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Nav {

  class Props(val activeKey: UndefOr[String] = undefined,
              val className: UndefOr[String] = undefined,
              val defaultActiveKey: UndefOr[String] = undefined,
              val fill: UndefOr[Boolean] = undefined,
              val justify: UndefOr[Boolean] = undefined,
              val onSelect: UndefOr[js.Function1[String, Unit]] = undefined,
              val variant: UndefOr[String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Nav")
  @js.native
  object RawComponent extends js.Object {
    val Item: js.Object = js.native
    val Link: js.Object = js.native
  }

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(activeKey: UndefOr[String] = undefined,
            className: UndefOr[String] = undefined,
            defaultActiveKey: UndefOr[String] = undefined,
            fill: Boolean = false,
            justify: Boolean = false,
            onSelect: String => Callback = _ => Callback.empty,
            variant: String = null)(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(
        activeKey,
        className,
        defaultActiveKey,
        fill,
        justify,
        js.defined(onSelect(_).runNow()),
        variant
      )
    )(content: _*)
  }
}
