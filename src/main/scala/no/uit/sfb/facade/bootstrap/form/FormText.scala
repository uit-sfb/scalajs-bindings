package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.Ref.Handle
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.raw.React.RefHandle
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import no.uit.sfb.facade.OptionToUndefOr._
import org.scalajs.dom.html

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object FormText {

  class Props(val muted: Boolean = false,
              val ref: UndefOr[RefHandle[html.Input]] = undefined)
      extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.Text)

  //Define the ref in your Backend class as follows:
  //private val ref: Simple[Input] = Ref[html.Input]
  def apply(ref: Option[Handle[html.Input]] = None,
            muted: Boolean = false)(content: VdomNode*): VdomElement =
    componentJS(new Props(muted, ref.map { _.raw }))(content: _*)
}
