package no.uit.sfb.facade.bootstrap.card

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Card {

  class Props(val bg: UndefOr[String] = undefined,
              val body: UndefOr[Boolean] = undefined,
              val border: UndefOr[String] = undefined,
              val text: UndefOr[String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Card")
  @js.native
  object RawComponent extends js.Object {
    val Body: js.Object = js.native
    val Header: js.Object = js.native
  }

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    bg: UndefOr[String] = undefined,
    body: Boolean = false,
    border: UndefOr[String] = undefined,
    text: UndefOr[String] = undefined
  )(content: VdomNode*): VdomElement = {
    componentJS(new Props(bg, body, border, text))(content: _*)
  }
}
