package no.uit.sfb.facade.bootstrap.modal

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

//Not working!
//Cf https://github.com/scala-js/scala-js/issues/4427
@deprecated("Not working!")
object ModalHeader {

  class Props(val closeButton: UndefOr[Boolean] = undefined,
              val onHide: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
  ) extends js.Object

  @JSImport("react-bootstrap", "ModalHeader")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    closeButton: Boolean = false,
    onHide: ReactEvent => Callback = _ => Callback.empty
  )(content: VdomNode*): VdomElement =
    componentJS(new Props(closeButton, js.defined(onHide(_).runNow())))(
      content: _*
    )
}
