package no.uit.sfb.facade.bootstrap.accordion

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js._

object AccordionToggle {

  class Props(val eventKey: UndefOr[String] = undefined,
              val onClick: UndefOr[js.Function1[ReactEvent, Unit]] = undefined,
              val variant: UndefOr[String] = undefined)
      extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Accordion.RawComponent.Toggle)

  def apply(
    eventKey: String,
    onClick: ReactEvent => Callback = _ => Callback.empty,
    variant: js.UndefOr[String] = undefined
  )(content: VdomNode*): VdomElement =
    componentJS(new Props(eventKey, js.defined(onClick(_).runNow()), variant))(
      content: _*
    )
}
