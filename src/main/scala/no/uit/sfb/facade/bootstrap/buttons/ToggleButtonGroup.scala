package no.uit.sfb.facade.bootstrap.buttons

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{
  Callback,
  Children,
  CtorType,
  JsComponent,
  ReactEventFromInput
}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object ToggleButtonGroup {

  class Props(val name: UndefOr[String] = undefined,
              val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] =
                undefined,
              val size: UndefOr[String] = undefined,
              val `type`: UndefOr[String] = undefined,
              val value: UndefOr[String] = undefined,
              val vertical: UndefOr[Boolean] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "ToggleButtonGroup")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(name: String,
            onChange: ReactEventFromInput => Callback = _ => Callback.empty,
            size: UndefOr[String] = undefined,
            `type`: UndefOr[String] = undefined,
            value: UndefOr[String] = undefined,
            vertical: Boolean = false)(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(name,
                js.defined(onChange(_).runNow()),
                size,
                `type`,
                value,
                vertical))(content: _*)
  }
}
