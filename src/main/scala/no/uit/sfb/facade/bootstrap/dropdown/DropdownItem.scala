package no.uit.sfb.facade.bootstrap.dropdown

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object DropdownItem {

  class Props(val active: UndefOr[Boolean] = undefined,
              val disabled: UndefOr[Boolean] = undefined,
              val download: UndefOr[String] = undefined,
              val eventKey: UndefOr[String] = undefined,
              val href: UndefOr[String] = undefined,
              val onClick: UndefOr[js.Function1[ReactEvent, Unit]] = undefined)
      extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Dropdown.RawComponent.Item)

  def apply(
    active: Boolean = false,
    disabled: Boolean = false,
    download: UndefOr[String] = undefined,
    eventKey: UndefOr[String] = undefined,
    href: UndefOr[String] = undefined,
    onClick: ReactEvent => Callback = _ => Callback.empty
  )(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(
        active,
        disabled,
        download,
        eventKey,
        href,
        js.defined(onClick(_).runNow())
      )
    )(content: _*)
  }
}
