package no.uit.sfb.facade.bootstrap.grid

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Row {

  class Props(val className: UndefOr[String] = undefined,
              val lg: UndefOr[String] = undefined,
              val md: UndefOr[String] = undefined,
              val noGutters: UndefOr[Boolean] = undefined,
              val sm: UndefOr[String] = undefined,
              val xl: UndefOr[String] = undefined,
              val xs: UndefOr[String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Row")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    className: UndefOr[String] = undefined, //Ex: justify-content-between (cf https://getbootstrap.com/docs/5.0/utilities/flex/#justify-content)
    lg: UndefOr[String] = undefined,
    md: UndefOr[String] = undefined,
    noGutters: Boolean = false,
    sm: UndefOr[String] = undefined,
    xl: UndefOr[String] = undefined,
    xs: UndefOr[String] = undefined
  )(content: VdomNode*): VdomElement = {
    componentJS(new Props(className, lg, md, noGutters, sm, xl, xs))(
      content: _*
    )
  }
}
