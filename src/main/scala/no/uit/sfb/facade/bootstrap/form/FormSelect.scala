package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js.{UndefOr, undefined}

//Importing Form.Select or FormSelect didn't work, so we just use FormControl
object FormSelect {

  /*class Props(val as: String = "select",
              val disabled: UndefOr[Boolean] = undefined,
              val htmlSize: UndefOr[Int] = undefined,
              val id: UndefOr[String] = undefined,
              val isInvalid: UndefOr[Boolean] = undefined,
              val isValid: UndefOr[Boolean] = undefined,
              val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] =
                undefined,
              val size: UndefOr[String] = undefined,
              val value: UndefOr[String] = undefined)
      extends js.Object*/

  /*@JSImport("react-bootstrap", "FormSelect")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent)*/

  def apply(
    disabled: Boolean = false,
    htmlSize: UndefOr[Int] = undefined,
    id: UndefOr[String] = undefined, //Not needed in most cases FormGroup already sets an id automatically
    isInvalid: Boolean = false,
    isValid: Boolean = false,
    onChange: ReactEventFromInput => Callback = _ => Callback.empty,
    size: UndefOr[String] = undefined, //sm | lg
    value: String = ""
  )(content: VdomNode*): VdomElement =
    FormControl(
      as = "select",
      disabled = disabled,
      htmlSize = htmlSize,
      id = id,
      isValid = isValid,
      isInvalid = isInvalid,
      onChange = onChange,
      size = size,
      value = value
    )(content: _*)
}
