package no.uit.sfb.facade.bootstrap.card

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js

object CardHeader {

  class Props() extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Card.RawComponent.Header)

  def apply()(content: VdomNode*): VdomElement =
    componentJS(new Props())(content: _*)
}
