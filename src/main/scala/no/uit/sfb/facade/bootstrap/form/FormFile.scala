package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react._

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object FormFile {

  class Props(val disabled: Boolean,
              val id: UndefOr[String] = undefined,
              val label: UndefOr[String] = undefined,
              val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] =
                undefined,
              val required: Boolean = false)
      extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.File)

  def apply(
    disabled: Boolean = false,
    id: UndefOr[String] = undefined, //Not needed in most cases FormGroup already sets an id automatically
    label: String = "",
    onChange: ReactEventFromInput => Callback = _ => Callback.empty,
    required: Boolean = false
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        disabled,
        id,
        label,
        onChange = js.defined(onChange(_).runNow()),
        required
      )
    )(content: _*)
}
