package no.uit.sfb.facade.bootstrap.buttons

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{
  Callback,
  Children,
  CtorType,
  JsComponent,
  ReactEvent
}
import no.uit.sfb.facade.OptionToUndefOr._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Button {

  class Props(val active: UndefOr[Boolean] = undefined,
              val block: UndefOr[Boolean] = undefined,
              val disabled: UndefOr[Boolean] = undefined,
              val download: UndefOr[String] = undefined,
              val href: UndefOr[String] = undefined,
              val size: UndefOr[String] = undefined,
              val `type`: UndefOr[String] = undefined,
              val variant: UndefOr[String] = undefined,
              val onClick: UndefOr[js.Function1[ReactEvent, Unit]])
      extends js.Object

  @JSImport("react-bootstrap", "Button")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    active: Boolean = false,
    block: Boolean = false,
    disabled: Boolean = false,
    download: Option[String] = None, //Cf https://www.w3schools.com/howto/howto_html_download_link.asp Will only work if same origin.
    href: UndefOr[String] = undefined,
    size: UndefOr[String] = undefined, //sm | lg
    `type`: String = "button",
    variant: String = "primary",
    onClick: ReactEvent => Callback = _ => Callback.empty //React event needed if one needs to preventDefault or stopPropagation
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        active,
        block,
        disabled,
        download,
        href,
        size,
        `type`,
        variant,
        js.defined(onClick(_).runNow())
      )
    )(content: _*)
}
