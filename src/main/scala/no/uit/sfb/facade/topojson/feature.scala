package no.uit.sfb.facade.topojson

import no.uit.sfb.facade.geojson.Topology

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@JSImport("topojson-client", "feature")
@js.native
object feature extends js.Object {
  def apply(
    topology: Topology,
    countries: js.Object
  ): js.Function2[js.Object, js.Object, js.Object] =
    js.native
}
