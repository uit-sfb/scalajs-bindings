package no.uit.sfb.facade.geojson

import no.uit.sfb.facade.d3.geo.GeoJson

import scala.scalajs.js

class FeatureProperty(val name: js.UndefOr[String],
                      val gaz: js.UndefOr[String],
) extends js.Object

class Geometry(val `type`: String, val coordinates: js.Array[Any])
    extends js.Object

class GeometryCollection(val `type`: String,
                         val geometries: js.Array[js.Object],
) extends js.Object

class Transform(val scale: js.Array[Int], val translate: js.Array[Int])

class Topology(val arcs: js.Array[js.Array[js.Array[Int]]],
               val bbox: js.Array[Int],
               val objects: TopoObjects,
               val transform: Transform,
               val `type`: String)
    extends js.Object

class TopoObjects(val main: GeometryCollection) extends js.Object

class Feature(val `type`: String,
              val id: Any,
              val bbox: js.Array[Int],
              val properties: FeatureProperty,
              val geometry: Geometry)
    extends js.Object

class FeatureCollection(val features: js.Array[Feature]) extends GeoJson

case class StrokeStyle(strokeColor: String = "#FFFFFF",
                       strokeWidth: Double = 0.5)
