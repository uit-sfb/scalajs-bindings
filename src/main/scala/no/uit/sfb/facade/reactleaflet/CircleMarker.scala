package no.uit.sfb.facade.reactleaflet

import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent, ReactEvent}
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import no.uit.sfb.facade.reactleaflet.options.PositionOption

import scala.scalajs.js
import scala.scalajs.js.UndefOr
import scala.scalajs.js.annotation.JSImport

object CircleMarker {

  type OnMouseOver = js.Function1[ReactEvent, Unit]
  type OnMouseOut = js.Function1[ReactEvent, Unit] //we create types this way to implement onValueMouse as function instead of js.Object

  class Props(
               val center: UndefOr[PositionOption] = js.undefined,
               val radius: js.UndefOr[Int] = js.undefined,
               val weight: js.UndefOr[Int] = js.undefined,
               val stroke: js.UndefOr[Boolean] = js.undefined,
               val color: js.UndefOr[String] = js.undefined,
               val opacity: js.UndefOr[Double] = js.undefined,
               val fillColor: js.UndefOr[String] = js.undefined,
               val fillOpacity: js.UndefOr[Double] = js.undefined,
               val onMouseOver: UndefOr[OnMouseOver] = js.undefined,
               val eventHandlers: UndefOr[EventObject] = js.undefined,
               val onMouseOut: UndefOr[OnMouseOut] = js.undefined,
             ) extends js.Object


  @JSImport("react-leaflet", "CircleMarker")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
             center: UndefOr[PositionOption] = js.undefined,
             radius: js.UndefOr[Int] = 5,
             weight: js.UndefOr[Int] = 2,
             stroke: js.UndefOr[Boolean] = true,
             color: js.UndefOr[String] = "#FFFFFF",
             opacity: js.UndefOr[Double] = 1.0,
             fillColor: js.UndefOr[String] = "#4169E1",
             fillOpacity: js.UndefOr[Double] = 1.0,
             onMouseOver: ReactEvent => Callback = _ => Callback.empty,
             eventHandlers: UndefOr[EventObject] = js.undefined,
             onMouseOut: ReactEvent => Callback = _ => Callback.empty,
           )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(
          center,
          radius,
          weight,
          stroke,
          color,
          opacity,
          fillColor,
          fillOpacity,
          onMouseOver = js.defined(onMouseOver(_).runNow()),
          eventHandlers,
          onMouseOut = js.defined(onMouseOut(_).runNow())
        ))(content: _*)
    }
  }

}
