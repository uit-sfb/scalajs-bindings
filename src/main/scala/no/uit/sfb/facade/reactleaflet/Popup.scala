package no.uit.sfb.facade.reactleaflet

import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}

import scala.scalajs.js
import scala.scalajs.js.UndefOr
import scala.scalajs.js.annotation.JSImport

object Popup {

  class Props(
             ) extends js.Object


  @JSImport("react-leaflet", "Popup")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
           )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(
        ))(content: _*)
    }
  }

}
