package no.uit.sfb.facade.reactleaflet

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import org.scalajs.dom.Element

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object TileLayer {

  class Props(
             val attribution: UndefOr[String] = js.undefined,
             val url: UndefOr[String] = js.undefined
             ) extends js.Object


  @JSImport("react-leaflet", "TileLayer")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
             attribution: UndefOr[String] = js.undefined,
             url: UndefOr[String] = js.undefined
           )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(
          attribution,
          url
        ))(content: _*)
    }
  }

}