package no.uit.sfb.facade.reactleaflet

import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent, ReactEvent}
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import no.uit.sfb.facade.reactleaflet.options.PositionOption

import scala.scalajs.js
import scala.scalajs.js.UndefOr
import scala.scalajs.js.annotation.JSImport

object Marker {

  type OnMouseOver = js.Function1[ReactEvent, Unit]
  type OnMouseOut = js.Function1[ReactEvent, Unit] //we create types this way to implement onValueMouse as function instead of js.Object

  class Props(
               val position: UndefOr[PositionOption] = js.undefined,
               val onMouseOver: UndefOr[OnMouseOver] = js.undefined,
               val onMouseOut: UndefOr[OnMouseOut] = js.undefined,
             ) extends js.Object


  @JSImport("react-leaflet", "Marker")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
             position: UndefOr[PositionOption] = js.undefined,
             onMouseOver: ReactEvent => Callback = _ => Callback.empty,
             onMouseOut: ReactEvent => Callback = _ => Callback.empty,
           )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(
          position,
          onMouseOver = js.defined(onMouseOver(_).runNow()),
          onMouseOut = js.defined(onMouseOut(_).runNow())
        ))(content: _*)
    }
  }

}
