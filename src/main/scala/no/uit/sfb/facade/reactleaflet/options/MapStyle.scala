package no.uit.sfb.facade.reactleaflet.options

import scala.scalajs.js

class MapStyle(val height: js.UndefOr[String] = js.undefined,
               val width: js.UndefOr[String] = js.undefined,
) extends js.Object

object MapStyle {
  def apply(height: String) = new MapStyle(height)
  def apply(height: String, width: String) = new MapStyle(height, width)
}
