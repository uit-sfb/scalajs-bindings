package no.uit.sfb.facade.reactleaflet

import japgolly.scalajs.react.{Callback, ReactEvent}

import scala.scalajs.js

class EventObject(
  val click: js.UndefOr[js.Function1[ReactEvent, Unit]] = js.undefined
) extends js.Object {}

object EventObject {
  def apply(click: ReactEvent => Callback = _ => Callback.empty): EventObject =
    new EventObject(click = js.defined(click(_).runNow()))

}
