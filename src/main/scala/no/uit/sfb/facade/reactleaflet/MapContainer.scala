package no.uit.sfb.facade.reactleaflet

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import org.scalajs.dom.Element

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}
import options._
import org.scalajs.dom

object MapContainer {

  type Target = String | Element

  class Props(val center: UndefOr[PositionOption] = js.undefined,
              val zoom: UndefOr[Int] = js.undefined,
              val scrollWheelZoom: UndefOr[Boolean] = js.undefined,
              val style: UndefOr[MapStyle] = js.undefined)
      extends js.Object
  @JSImport("react-leaflet", "MapContainer")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
    center: (Double, Double) = (51.505, -0.09),
    zoom: Int = 2,
    scrollWheelZoom: Boolean = true,
    height: Int = 600,
    width: Int = 0 //If width <= 0, width adapts to the window. For iFrame embedding, use dom.window.innerWidth
  )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(
          PositionOption(center._1, center._2),
          zoom,
          scrollWheelZoom,
          if (width > 0)
            MapStyle(s"${height}px", s"${width}px")
          else
            MapStyle(s"${height}px")
        )
      )(content: _*)
    }
  }

}
