package no.uit.sfb.facade.reactleaflet.options

import scala.scalajs.js

class PositionOption(
                  val lat: Double,
                  val lng: Double
                ) extends js.Object


object PositionOption {
  def apply(lat: Double,lng: Double):PositionOption = new PositionOption(lat,lng)
  def apply(latlon:String):PositionOption = {
    val latLonVals = latlon.split(',').map(_.toDouble)
    new PositionOption(latLonVals.head,latLonVals.last)
  }
}
