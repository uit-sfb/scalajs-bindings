package no.uit.sfb.facade.reactleaflet.options

import scala.scalajs.js

class BoolOption(
                         val value: Boolean,
                       ) extends js.Object


object BoolOption {
  def apply(value:Boolean = false) = new BoolOption(value)
}
