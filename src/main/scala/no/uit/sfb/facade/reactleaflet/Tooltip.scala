package no.uit.sfb.facade.reactleaflet

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

object Tooltip {

  class Props(
    val onOpen: js.UndefOr[js.Function1[ReactEvent, Unit]] = js.undefined,
    val onClose: js.UndefOr[js.Function1[ReactEvent, Unit]] = js.undefined
  ) extends js.Object
  @JSImport("react-leaflet", "Tooltip")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(onOpen: ReactEvent => Callback = _ => Callback.empty,
            onClose: ReactEvent => Callback = _ => Callback.empty,
  )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(
          onOpen = js.defined(onOpen(_).runNow()),
          onClose = js.defined(onClose(_).runNow()),
        )
      )(content: _*)
    }
  }
}
