package no.uit.sfb.facade.reactvis.series

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import no.uit.sfb.facade.reactvis.obj.LabelData

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

object LabelSeries {

  class Props(val labelAnchorX: String,
              val labelAnchorY: String,
              val data: js.Array[LabelData],
  ) extends js.Object
  @JSImport("react-vis", "LabelSeries")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      labelAnchorX: String,
      labelAnchorY: String,
      data: Iterable[LabelData],
  )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(labelAnchorX, labelAnchorY, js.Array(data.toSeq: _*)))(
        content: _*)
    }
  }
}
