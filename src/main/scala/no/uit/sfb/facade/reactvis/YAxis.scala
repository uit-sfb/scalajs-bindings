package no.uit.sfb.facade.reactvis

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.all.VdomNode
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import no.uit.sfb.facade.d3.format.Format

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object YAxis {

  class Props(
      val title: UndefOr[String] = undefined,
      val position: UndefOr[String] = undefined,
      val tickLabelAngle: UndefOr[Int] = undefined,
      val tickFormat: UndefOr[js.Function1[Double | Int | String, String]] =
        undefined
  ) extends js.Object

  @JSImport("react-vis", "YAxis")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      title: UndefOr[String] = undefined,
      position: String = "middle", //start, middle, end
      tickLabelAngle: Int = 0,
      scientificNotation: Boolean = false,
      percentNotation: Boolean = false
  )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(
          title,
          position,
          tickLabelAngle,
          if (scientificNotation)
            js.defined(Format(".2s")
              .asInstanceOf[js.Function1[Double | Int | String, String]])
          else if (percentNotation)
            js.defined(Format(".0%")
              .asInstanceOf[js.Function1[Double | Int | String, String]])
          else
            undefined
        ))(content: _*)
    }
  }

}
