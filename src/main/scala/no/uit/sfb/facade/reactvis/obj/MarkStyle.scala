package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js

class MarkStyle(val stroke: String) extends js.Object

object MarkStyle {
  def apply(stroke: String): MarkStyle = new MarkStyle(stroke)
}
