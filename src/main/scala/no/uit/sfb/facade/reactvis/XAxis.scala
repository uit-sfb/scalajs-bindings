package no.uit.sfb.facade.reactvis

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.all.VdomNode
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import no.uit.sfb.facade.d3.format.Format

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object XAxis {

  class Props(
      val title: UndefOr[String],
      val position: UndefOr[String],
      val tickLabelAngle: UndefOr[Int],
      val tickFormat: UndefOr[js.Function1[Double | Int | String, String]] =
        undefined
  ) extends js.Object

  @JSImport("react-vis", "XAxis")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      title: UndefOr[String] = undefined,
      position: String = "middle", //start, middle, end
      tickLabelAngle: Int = 0,
      scientificNotation: Boolean = false,
      cutString: Int = 0 //Max number of characters (if <= 0, disable cutout)
  )(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(
          title,
          position,
          tickLabelAngle,
          if (scientificNotation)
            js.defined(Format(".2s")
              .asInstanceOf[js.Function1[Double | Int | String, String]])
          else if (cutString > 0)
            js.defined(str =>
              if (str.toString.length > cutString)
                str.toString.take(cutString) + "..."
              else
                str.toString
            )
          else
            undefined
          ))(content: _*)
    }
  }

}
