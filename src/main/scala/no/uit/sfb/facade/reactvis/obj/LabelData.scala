package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js

class LabelData(val data: Point, val label: String) extends js.Object

object LabelData {
  def apply(data: (Any, Any), label: String): LabelData =
    new LabelData(new Point(data._1, data._2), label)
}
