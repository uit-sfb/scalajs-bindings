package no.uit.sfb.facade.reactvis

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object XYPlot {

  class Props(
      val margin: UndefOr[js.Object] = undefined,
      val xType: UndefOr[String] = undefined,
      val yType: UndefOr[String] = undefined,
      val xDomain: UndefOr[js.Array[Int]] = undefined,
      val yDomain: UndefOr[js.Array[Int]] = undefined,
      val height: UndefOr[Int] = undefined,
      val colorType: UndefOr[String] = undefined,
      val colorDomain: UndefOr[Seq[Int]] = undefined
  ) extends js.Object
  class Props2(
      val margin: UndefOr[js.Object] = undefined,
      val xType: UndefOr[String] = undefined,
      val yType: UndefOr[String] = undefined,
      val xDomain: UndefOr[js.Array[Int]] = undefined,
      val yDomain: UndefOr[js.Array[Int]] = undefined,
      val height: UndefOr[Int] = undefined,
      val width: Int, //Doesn't work with undefined
      val colorType: UndefOr[String] = undefined,
      val colorDomain: UndefOr[Seq[Int]] = undefined,
      val onMouseDown: UndefOr[js.Function1[ReactEvent, Unit]] = undefined
  ) extends js.Object

  @JSImport("react-vis", "XYPlot")
  @js.native
  object RawComponent extends js.Object

  @JSImport("react-vis", "FlexibleXYPlot")
  @js.native
  object RawComponentFlex extends js.Object

  private val componentJS: Component[Props2, Null, CtorType.PropsAndChildren] =
    JsComponent[Props2, Children.Varargs, Null](RawComponent)

  private val componentJSFlex
    : Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponentFlex)

  def apply(
      height: Int = 600,
      width: Option[Int] = None,
      margin: UndefOr[js.Object] = undefined, //Use js.Dynamic.literal(left = 100, bottom = 100)
      xType: UndefOr[String] = undefined,
      yType: UndefOr[String] = undefined,
      xDomain: UndefOr[Seq[Int]] = undefined,
      yDomain: UndefOr[Seq[Int]] = undefined,
      colorType: UndefOr[String] = undefined,
      colorDomain: UndefOr[Seq[Int]] = undefined,
      onMouseDown: ReactEvent => Callback = _ => Callback.empty,
  )(content: VdomNode*): VdomElement = {
    {
      width match {
        case None =>
          val p = new Props(margin, xType, yType, xDomain.map {
            js.Array(_: _*)
          }, yDomain.map {
            js.Array(_: _*)
          }, height, colorType, colorDomain)
          componentJSFlex(p)(content: _*)
        case Some(w) =>
          val p = new Props2(
            margin,
            xType,
            yType,
            xDomain.map {
              js.Array(_: _*)
            },
            yDomain.map {
              js.Array(_: _*)
            },
            height,
            w,
            colorType,
            colorDomain,
            onMouseDown = js.defined(onMouseDown(_).runNow())
          )
          componentJS(p)(content: _*)
      }
    }
  }
}
