package no.uit.sfb.facade.reactvis.series

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent, ReactEvent}
import no.uit.sfb.facade.reactvis.obj.Point

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object VerticalBarSeries {

  @JSImport("react-vis", "VerticalBarSeries")
  @js.native
  object RawComponent extends js.Object

  type OnValueMouseOver = js.Function2[Point, ReactEvent, Unit]
  type OnSeriesMouseOut = js.Function1[ReactEvent, Unit]

  class Props(val color: UndefOr[String] = undefined,
              val opacity: UndefOr[Double] = undefined,
              val stroke: UndefOr[Int] = undefined,
              val fill: UndefOr[Int] = undefined,
              val style: UndefOr[js.Object] = undefined,
              val onValueMouseOver: UndefOr[OnValueMouseOver] = undefined,
              val onSeriesMouseOut: UndefOr[OnSeriesMouseOut] = undefined,
              val data: js.Array[Point]
             ) extends js.Object {
  }

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)


  def apply(color: UndefOr[String] = undefined,
            opacity: UndefOr[Double] = undefined,
            stroke: UndefOr[Int] = undefined,
            fill: UndefOr[Int] = undefined,
            style: UndefOr[Any] = undefined,
            onValueMouseOver: (Point, ReactEvent) => Callback = (_,_) => Callback.empty,
            onSeriesMouseOut: ReactEvent => Callback = _ => Callback.empty,
            data: Seq[Point])(content: VdomNode*): VdomElement = {
    {
      componentJS(new Props(color, opacity, stroke, fill, style.map {
        js.Object(_)
      },
        onValueMouseOver = js.defined(onValueMouseOver(_,_).runNow()),
        onSeriesMouseOut = js.defined(onSeriesMouseOut(_).runNow()),
        data = js.Array(data: _*)))(content: _*)
    }
  }
}
