package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js
import scala.scalajs.js._

class RadialData(val angle: Double,
                 val radius: UndefOr[Int] = undefined,
                 val label: UndefOr[String] = undefined,
                 val sublabel: UndefOr[String] = undefined,
                 val value: UndefOr[String] = undefined, // meant for storing the angle as the angle field is changed by react-vis
                 val color: UndefOr[String | Double | Int] = undefined,
                 val style: UndefOr[js.Object] = undefined,
                 val className: UndefOr[String] = undefined,
                 val padAngle: UndefOr[Double] = undefined,
                 val angle0: UndefOr[Double] = undefined,
                 val radius0: UndefOr[Int] = undefined,
                 val x: UndefOr[Double] = undefined,
                 val y: UndefOr[Double] = undefined,
                ) extends js.Object
