package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js

class LineStyle(val stroke: String) extends js.Object

object LineStyle {
  def apply(stroke: String): LineStyle = new LineStyle(stroke)
}
