package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

class LegendData(
                  val title: UndefOr[String] = undefined,
                  val color: UndefOr[String] = undefined,
                  val strokeDasharray: UndefOr[String] = undefined,
                  val strokeStyle: UndefOr[String] = undefined,
                  val strokeWidth: UndefOr[Int] = undefined,
                  val disabled: UndefOr[Boolean] = undefined
                ) extends js.Object

object LegendData{
  def apply(
             title: UndefOr[String] = undefined,
             color: UndefOr[String] = undefined,
             strokeDasharray: UndefOr[String] = undefined,
             strokeStyle: UndefOr[String] = undefined,
             strokeWidth: UndefOr[Int] = undefined,
             disabled: UndefOr[Boolean] = undefined
           ): LegendData = new LegendData(title,color,strokeDasharray,strokeStyle,strokeWidth,disabled)
}