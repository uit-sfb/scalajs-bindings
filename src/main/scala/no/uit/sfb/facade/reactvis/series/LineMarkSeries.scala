package no.uit.sfb.facade.reactvis.series

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.all.VdomNode
import no.uit.sfb.facade.reactvis.obj.Point

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object LineMarkSeries {

  @JSImport("react-vis", "LineMarkSeries")
  @js.native
  object RawComponent extends js.Object

  type OnValueMouseOver = js.Function2[Point, ReactEvent, Unit]
  type OnSeriesMouseOut = js.Function1[ReactEvent, Unit] //we create types this way to implement onValueMouse as function instead of js.Object

  class Props(
      val color: UndefOr[String | Int] = undefined,
      val title: UndefOr[String] = undefined,
      val curve: UndefOr[String] = undefined,
      val opacity: UndefOr[Double] = undefined,
      val stroke: UndefOr[String] = undefined,
      val strokeStyle: UndefOr[String] = undefined,
      val strokeWidth: UndefOr[Int] = undefined,
      val style: UndefOr[js.Object] = undefined,
      val lineStyle: UndefOr[js.Object] = undefined,
      val markStyle: UndefOr[js.Object] = undefined,
      val onValueMouseOver: UndefOr[OnValueMouseOver] = js.undefined,
      val onValueMouseOut: UndefOr[OnValueMouseOver] = js.undefined,
      val onSeriesMouseOut: UndefOr[OnSeriesMouseOut] = js.undefined,
      val data: js.Array[Point] = js.Array()
  ) extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      color: UndefOr[String | Int] = undefined,
      title: UndefOr[String] = undefined,
      curve: UndefOr[String] = undefined,
      opacity: UndefOr[Double] = undefined,
      stroke: UndefOr[String] = undefined,
      strokeStyle: UndefOr[String] = undefined,
      strokeWidth: UndefOr[Int] = undefined,
      style: UndefOr[Any] = undefined,
      lineStyle: UndefOr[Any] = undefined,
      markStyle: UndefOr[Any] = undefined,
      onValueMouseOver: (Point, ReactEvent) => Callback = (_, _) =>
        Callback.empty,
      onValueMouseOut: (Point, ReactEvent) => Callback = (_, _) =>
        Callback.empty,
      //onSeriesMouseOut: ReactEvent => Callback = _ => Callback.empty,
      data: Seq[Point]
  )(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(
        color,
        title,
        curve,
        opacity,
        stroke,
        strokeStyle,
        strokeWidth,
        style.map {
          js.Object(_)
        },
        lineStyle.map {
          js.Object(_)
        },
        markStyle.map {
          js.Object(_)
        },
        onValueMouseOver = js.defined(onValueMouseOver(_, _).runNow()),
        onValueMouseOut = js.defined(onValueMouseOut(_, _).runNow()),
        //onSeriesMouseOut = js.defined(onSeriesMouseOut(_).runNow()),
        data = js.Array(data: _*)
      ))(content: _*)
  }

}
