package no.uit.sfb.facade.reactvis

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.all.VdomNode
import no.uit.sfb.facade.reactvis.obj.RadialData

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation.JSImport

object RadialChart {

  @JSImport("react-vis", "RadialChart")
  @js.native
  object RawComponent extends js.Object

  type OnValueMouseOver = js.Function2[RadialData, ReactEvent, Unit]
  type OnSeriesMouseOut = js.Function1[ReactEvent, Unit]

  class Props(
               val data: js.Array[RadialData],
               val height: Int,
               val width: Int,
               val getLabel: UndefOr[js.Function1[RadialData, String]] = undefined,
               val showLabels: UndefOr[Boolean] = true,
               val labelsAboveChildren: UndefOr[Boolean] = undefined,
               val labelsRadiusMultiplier: UndefOr[Double] = undefined, //Default: 1.1
               val innerRadius: UndefOr[Int] = undefined,
               val radius: UndefOr[Int] = undefined,
               val padAngle: UndefOr[Double] = undefined,
               val colorType: UndefOr[String] = undefined,
               //val colorDomain: UndefOr[js.Array[Double | Int]] = undefined,
               //val colorRange: UndefOr[js.Array[String]] = undefined,
               val onValueMouseOver: UndefOr[OnValueMouseOver] = undefined,
               val onSeriesMouseOut: UndefOr[OnSeriesMouseOut] = undefined,
             ) extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
             data: Seq[RadialData],
             height: Int = 500,
             width: Int = 500,
             showLabels: Boolean = true,
             innerRadius: Int = 100,
             radius: Int = 200,
             labelsRadiusMultiplier: Double = 1.1,
             padAngle: Double = 0.0,
             colorType: UndefOr[String] = undefined,
             //colorDomain: UndefOr[Seq[Double]] = undefined,
             //colorRange: UndefOr[Seq[String]] = undefined,
             onValueMouseOver: (RadialData, ReactEvent) => Callback = (_,_) => Callback.empty,
             onSeriesMouseOut: ReactEvent => Callback = _ => Callback.empty,
           )(content: VdomNode*): VdomElement = {
    componentJS(new Props(
      js.Array(data: _*),
      height,
      width,
      showLabels = js.defined(showLabels),
      innerRadius = innerRadius,
      radius = radius,
      padAngle = padAngle,
      labelsRadiusMultiplier = labelsRadiusMultiplier,
      colorType = colorType,
      //colorDomain = colorDomain.map(js.Array(_: _*)),
      //colorRange = colorRange.map(js.Array(_: _*)),
      onValueMouseOver = js.defined(onValueMouseOver(_, _).runNow()),
      onSeriesMouseOut = js.defined(onSeriesMouseOut(_).runNow())
    ))(content: _*)
  }
}
