package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js

class HintStyle(
               val visibility: String,
               val background: String,
               ) extends js.Object


object HintStyle {
  def apply(visibility: String = "hidden", background: String = "#000") = new HintStyle(visibility,background)
}


