package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js

class PlotStyle(val strokeWidth: Int) extends js.Object

object PlotStyle {
  def apply(strokeWidth: Int): PlotStyle = new PlotStyle(strokeWidth)
}
