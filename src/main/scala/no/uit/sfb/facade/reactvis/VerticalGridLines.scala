package no.uit.sfb.facade.reactvis

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.all.VdomNode
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object VerticalGridLines {

  class Props(
      val tickTotal: UndefOr[Int],
      val tickValues: UndefOr[js.Array[Double]],
      val left: UndefOr[Int],
      val top: UndefOr[Int],
      val width: UndefOr[Int],
      val height: UndefOr[Int],
      val className: UndefOr[String],
      val style: UndefOr[js.Object],
  ) extends js.Object

  @JSImport("react-vis", "VerticalGridLines")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      tickTotal: UndefOr[Int] = undefined,
      tickValues: UndefOr[Seq[Double]] = undefined,
      left: UndefOr[Int] = undefined,
      top: UndefOr[Int] = undefined,
      width: UndefOr[Int] = undefined,
      height: UndefOr[Int] = undefined,
      className: UndefOr[String] = undefined,
      style: UndefOr[Any] = undefined,
  )(content: VdomNode*): VdomElement = {
    {
      componentJS(new Props(tickTotal, tickValues.map {
        js.Array(_: _*)
      }, left, top, width, height, className, style.map {
        js.Object(_)
      }))(content: _*)
    }
  }

}
