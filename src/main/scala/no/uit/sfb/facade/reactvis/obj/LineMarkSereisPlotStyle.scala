package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js

class LineMarkSereisPlotStyle(val strokeLinejoin: String, val strokeWidth: Int,
) extends js.Object

object LineMarkSereisPlotStyle {
  def apply(strokeLinejoin: String, strokeWidth: Int): LineMarkSereisPlotStyle =
    new LineMarkSereisPlotStyle(strokeLinejoin, strokeWidth)
}
