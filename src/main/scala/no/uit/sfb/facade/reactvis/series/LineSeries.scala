package no.uit.sfb.facade.reactvis.series

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.all.VdomNode
import no.uit.sfb.facade.reactvis.obj.Point

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object LineSeries {

  class Props(
      val data: js.Array[Point],
      val color: UndefOr[String],
      val curve: UndefOr[String],
      val opacity: UndefOr[Double],
      val stroke: UndefOr[String],
      val strokeStyle: UndefOr[String],
      val strokeWidth: UndefOr[Int],
      val style: UndefOr[js.Object],
  ) extends js.Object
  @JSImport("react-vis", "LineSeries")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      data: Iterable[(Any, Any)],
      color: UndefOr[String] = undefined,
      curve: UndefOr[String] = undefined,
      opacity: UndefOr[Double] = undefined,
      stroke: UndefOr[String] = undefined,
      strokeStyle: UndefOr[String] = undefined,
      strokeWidth: UndefOr[Int] = undefined,
      style: UndefOr[Any] = undefined,
  )(content: VdomNode*): VdomElement = {
    componentJS(new Props(js.Array(data.toSeq.map {
      case (x, y) => new Point(x, y)
    }: _*), color, curve, opacity, stroke, strokeStyle, strokeWidth, style.map {
      js.Object(_)
    }))(content: _*)
  }
}
