package no.uit.sfb.facade.reactvis.obj

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

class Point(
    val x: Any,
    val y: Any,
    val x0: UndefOr[Any] = undefined, //Used to store the original x when a transformation is necessary
    val y0: UndefOr[Any] = undefined, //Used to store the original y when a transformation is necessary
    val opacity: UndefOr[Double] = 1.0,
    val label: UndefOr[String] = undefined //Used to store the label of the graph when multiple graphs are displayed at once
) extends js.Object
