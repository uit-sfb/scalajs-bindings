package no.uit.sfb.facade.reactvis

object Palette {
  //React-vis bug: when setting the color in a colorDomain, it always appear black
  //So we have to do the coloring ourselves
  val palette = Array(
    "#12939a",
    "#ff9933",
    "#79c7e3",
    "#ef5d28",
    "#1a3177"
  )

  def apply(i: Int): String = {
    palette(i % palette.length)
  }
}
