package no.uit.sfb.facade.reactvis.legends

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.all.VdomNode
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import no.uit.sfb.facade.reactvis.obj.LegendData

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation.JSImport

object DiscreteColorLegend {
  class Props(
               val orientation: UndefOr[String],
               val items: js.Array[LegendData] = js.Array(),
               val width: Int,
               val height: Int,
             ) extends js.Object

  @JSImport("react-vis", "DiscreteColorLegend")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)


  def apply(
             orientation: UndefOr[String] = js.defined("vertical"),
             items: Iterable[Any],
             width: Int,
             height: Int,
           )(content: VdomNode*): VdomElement = {
    {
      componentJS(new Props(
        orientation,
        js.Array(
          items.toSeq.map {
            case ((x,_),z) => LegendData(title = x.toString,color = z.toString)
          }: _*
      ),
        width,
        height
      ))(content: _*)
    }
  }
}
