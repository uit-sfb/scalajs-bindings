package no.uit.sfb.facade.reactvis.series

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import no.uit.sfb.facade.reactvis.obj.Point

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object HorizontalBarSeries {

  class Props(
      val color: UndefOr[String] = undefined,
      val opacity: UndefOr[Double] = undefined,
      val stroke: UndefOr[Int] = undefined,
      val fill: UndefOr[Int] = undefined,
      val style: UndefOr[js.Object] = undefined,
      val data: UndefOr[js.Array[Point]] = undefined,
  ) extends js.Object

  @JSImport("react-vis", "HorizontalBarSeries")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      data: Iterable[(Any, Any)],
      color: UndefOr[String] = undefined,
      opacity: UndefOr[Double] = undefined,
      stroke: UndefOr[Int] = undefined,
      fill: UndefOr[Int] = undefined,
      style: UndefOr[Any] = undefined)(content: VdomNode*): VdomElement = {
    {
      componentJS(
        new Props(color,
                  opacity,
                  stroke,
                  fill,
                  style.map {
                    js.Object(_)
                  },
                  js.defined(js.Array(data.toSeq.map {
                    case (x, y) => new Point(x, y)
                  }: _*))))(content: _*)
    }
  }

}
