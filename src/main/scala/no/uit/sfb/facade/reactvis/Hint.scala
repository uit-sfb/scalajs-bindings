package no.uit.sfb.facade.reactvis

import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.vdom.all.VdomNode

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Hint {

  @JSImport("react-vis", "Hint")
  @js.native
  object RawComponent extends js.Object

  class Props(
             val value: js.Object = js.Object(),
             val align: UndefOr[js.Object] = undefined,
             val className: UndefOr[String] = undefined,
             val style: UndefOr[js.Object] = undefined
             ) extends js.Object


  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
           value: Any = None,
           align: UndefOr[Any] = undefined,
           className: UndefOr[String] = undefined,
           style: UndefOr[Any] = undefined
           )(content: VdomNode*): VdomElement = {
    componentJS(new Props(
      js.Object(value),
      align.map{js.Object(_)},
      className,
      style.map{js.Object(_)}
    ))(content: _*)
  }
}
