package no.uit.sfb.facade.d3.scale

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

// from https://github.com/fdietze/scala-js-d3v4
@JSImport("d3-scale", JSImport.Namespace)
@js.native
object Scale extends js.Object {
  def scaleLinear(): LinearScale = js.native

  @js.native
  trait Scale extends js.Object

  @js.native
  trait ContinuousScale[S <: ContinuousScale[S]] extends Scale {
    def apply(value: js.Any): js.Any = js.native

    def invert(value: Double): Double = js.native

    def domain(domain: js.Array[Double]): S = js.native

    def range(range: js.Array[js.Any]): S = js.native
  }

  @js.native
  trait LinearScale extends ContinuousScale[LinearScale] {
  }

}
