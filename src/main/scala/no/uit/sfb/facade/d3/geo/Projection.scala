package no.uit.sfb.facade.d3.geo

import scala.scalajs.js

//Documentation: https://github.com/d3/d3-geo

@js.native
trait Projection extends js.Object {
  //transform lat, long in degree -> px
  def apply(v: js.Tuple2[Double, Double]): js.Tuple2[Int, Int]
  //Reverse transform (px -> lat, long in degrees)
  def invert(v: js.Tuple2[Int, Int]): js.Tuple2[Double, Double]

  def scale(scale: Double): this.type

  //translate center in px
  def translate(v: js.Tuple2[Int, Int]): this.type

  //Defines projection center
  def center(v: js.Tuple2[Double, Double]): this.type

  //Post-project planar rotation (in degrees)
  def angle(v: Double): this.type

  //All angles in degrees
  //right, up
  def rotate(v: js.Tuple2[Double, Double]): this.type

  //All angles in degrees
  //right, up, anti-clockwise
  def rotate(v: js.Tuple3[Double, Double, Double]): this.type

  //Set scale and translate to set center at the middle of the extent
  def fitExtent(extent: js.Tuple2[js.Tuple2[Int, Int], js.Tuple2[Int, Int]], obj: GeoJson): this.type
  //Same as fitExtent but with top-left corner set to [0, 0]
  def fitSize(size: js.Tuple2[Int, Int], obj: GeoJson): this.type
  //Same as fitSize but with automatic width to preserve aspect ratio
  def fitHeight(height: Int, obj: GeoJson): this.type
  //Same as fitSize but with automatic height to preserve aspect ratio
  def fitWidth(width: Int, obj: GeoJson): this.type
}
