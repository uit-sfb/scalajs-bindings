package no.uit.sfb.facade.d3.geo

import no.uit.sfb.facade.geojson.Feature

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@js.native
trait Path extends js.Object {
  def projection(p: Projection): Path

  def centroid(f: Feature): js.Any

  def apply(obj: js.Any): js.Any = js.native
}

@JSImport("d3-geo", "geoPath")
@js.native
object GeoPath extends js.Object {
  def apply(p: Projection): Path = js.native

  def centroid(f: Feature): js.Any = js.native
}
