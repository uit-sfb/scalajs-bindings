package no.uit.sfb.facade.d3.geo.projection

import no.uit.sfb.facade.d3.geo.Projection

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@js.native
@JSImport("d3-geo", "geoNaturalEarth1")
object GeoNaturalEarth1 extends js.Object {
  def apply(): Projection = js.native
}
