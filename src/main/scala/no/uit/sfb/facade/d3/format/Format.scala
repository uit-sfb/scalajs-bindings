package no.uit.sfb.facade.d3.format

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@JSImport("d3-format", "format")
@js.native
object Format extends js.Object {
  def apply(specifier: String): js.Function1[Double, String] =
    js.native //https://github.com/d3/d3-format
}
