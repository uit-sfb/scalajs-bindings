package no.uit.sfb.facade.icon

import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.{Children, JsComponent}

import scala.collection.MapView
import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

//From https://react-icons.github.io/

object Glyphicon {

  class Props() extends js.Object

  @JSImport("react-icons/io", "IoIosGlobe")
  @js.native
  object IoIosGlobe extends js.Object

  @JSImport("react-icons/io", "IoMdGlobe")
  @js.native
  object IoMdGlobe extends js.Object

  @JSImport("react-icons/bs", "BsCaretUp")
  @js.native
  object BsCaretUp extends js.Object

  @JSImport("react-icons/bs", "BsCaretDown")
  @js.native
  object BsCaretDown extends js.Object

  @JSImport("react-icons/bs", "BsCaretLeft")
  @js.native
  object BsCaretLeft extends js.Object

  @JSImport("react-icons/bs", "BsCaretRight")
  @js.native
  object BsCaretRight extends js.Object

  @JSImport("react-icons/md", "MdViewCarousel")
  @js.native
  object MdViewCarousel extends js.Object

  @JSImport("react-icons/md", "MdViewList")
  @js.native
  object MdViewList extends js.Object

  @JSImport("react-icons/go", "GoGraph")
  @js.native
  object GoGraph extends js.Object

  @JSImport("react-icons/go", "GoInfo")
  @js.native
  object GoInfo extends js.Object

  @JSImport("react-icons/bs", "BsInfoSquare")
  @js.native
  object BsInfoSquare extends js.Object

  @JSImport("react-icons/ai", "AiOutlineQuestionCircle")
  @js.native
  object AiOutlineQuestionCircle extends js.Object

  @JSImport("react-icons/md", "MdAnnouncement")
  @js.native
  object MdAnnouncement extends js.Object

  @JSImport("react-icons/gr", "GrNext")
  @js.native
  object GrNext extends js.Object

  @JSImport("react-icons/ai", "AiOutlineCloudDownload")
  @js.native
  object AiOutlineCloudDownload extends js.Object

  @JSImport("react-icons/hi", "HiDocumentDownload")
  @js.native
  object HiDocumentDownload extends js.Object

  @JSImport("react-icons/io5", "IoOptionsOutline")
  @js.native
  object IoOptionsOutline extends js.Object

  @JSImport("react-icons/ai", "AiOutlineFunction")
  @js.native
  object AiOutlineFunction extends js.Object

  @JSImport("react-icons/bs", "BsClipboardData")
  @js.native
  object BsClipboardData extends js.Object

  @JSImport("react-icons/ai", "AiOutlineSetting")
  @js.native
  object AiOutlineSetting extends js.Object

  @JSImport("react-icons/bi", "BiColumns")
  @js.native
  object BiColumns extends js.Object

  @JSImport("react-icons/fi", "FiEdit")
  @js.native
  object FiEdit extends js.Object

  @JSImport("react-icons/ai", "AiOutlineLogin")
  @js.native
  object AiOutlineLogin extends js.Object

  @JSImport("react-icons/ai", "AiOutlineLogout")
  @js.native
  object AiOutlineLogout extends js.Object

  @JSImport("react-icons/ai", "AiOutlineFileUnknown")
  @js.native
  object AiOutlineFileUnknown extends js.Object

  @JSImport("react-icons/bs", "BsTrash")
  @js.native
  object BsTrash extends js.Object

  @JSImport("react-icons/md", "MdAddCircleOutline")
  @js.native
  object MdAddCircleOutline extends js.Object

  @JSImport("react-icons/bi", "BiUndo")
  @js.native
  object BiUndo extends js.Object

  @JSImport("react-icons/bi", "BiShow")
  @js.native
  object BiShow extends js.Object

  @JSImport("react-icons/gi", "GiKeyLock")
  @js.native
  object GiKeyLock extends js.Object

  @JSImport("react-icons/fi", "FiSend")
  @js.native
  object FiSend extends js.Object

  @JSImport("react-icons/bs", "BsShieldLockFill")
  @js.native
  object BsShieldLockFill extends js.Object

  @JSImport("react-icons/fi", "FiExternalLink")
  @js.native
  object FiExternalLink extends js.Object

  @JSImport("react-icons/ai", "AiOutlineCopy")
  @js.native
  object AiOutlineCopy extends js.Object

  @JSImport("react-icons/gr", "GrPlay")
  @js.native
  object GrPlay extends js.Object

  @JSImport("react-icons/gr", "GrResume")
  @js.native
  object GrResume extends js.Object

  @JSImport("react-icons/gr", "GrStop")
  @js.native
  object GrStop extends js.Object

  @JSImport("react-icons/fi", "FiCheck")
  @js.native
  object FiCheck extends js.Object

  @JSImport("react-icons/ai", "AiOutlineSearch")
  @js.native
  object AdvancedSearch extends js.Object

  private lazy val resources: MapView[String, VdomElement] = Map(
    "PreviousPage" -> BsCaretLeft,
    "NextPage" -> BsCaretRight,
    "Up" -> BsCaretUp,
    "Down" -> BsCaretDown,
    "ViewCarousel" -> MdViewCarousel,
    "ViewList" -> MdViewList,
    "Graph" -> GoGraph,
    "Info" -> GoInfo,
    "Info2" -> BsInfoSquare,
    "Help" -> AiOutlineQuestionCircle,
    "Announcement" -> MdAnnouncement,
    "Next" -> GrNext,
    "CloudDownload" -> AiOutlineCloudDownload,
    "DocumentDownload" -> HiDocumentDownload,
    "Options" -> IoOptionsOutline,
    "Settings" -> AiOutlineSetting,
    "Columns" -> BiColumns,
    "Function" -> AiOutlineFunction,
    "Data" -> BsClipboardData,
    "Globe" -> IoIosGlobe,
    "GeoGlobe" -> IoMdGlobe,
    "Edit" -> FiEdit,
    "Login" -> AiOutlineLogin,
    "Logout" -> AiOutlineLogout,
    "Unknown" -> AiOutlineFileUnknown,
    "Trash" -> BsTrash,
    "Add" -> MdAddCircleOutline,
    "Revert" -> BiUndo,
    "View" -> BiShow,
    "Send" -> FiSend,
    "Deprecated" -> BsShieldLockFill,
    "Unlock" -> GiKeyLock,
    "ExternalLink" -> FiExternalLink,
    "Copy" -> AiOutlineCopy,
    "Play" -> GrPlay,
    "Resume" -> GrResume,
    "Stop" -> GrStop,
    "Check" -> FiCheck,
    "AdvancedSearch" -> AdvancedSearch,
  ).view.mapValues { obj =>
    val comp = JsComponent[Props, Children.Varargs, Null](obj)
    comp(new Props())()
  }

  def apply(glyph: String): VdomElement = {
    resources.getOrElse(glyph, resources("Unknown"))
  }
}
