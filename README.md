# Scalajs bindings

ScalaJS facades for some common javascript libraries.

## Usage

In build.sbt, add the following dependency: `"no.uit.sfb" %%% "scalajs-bindings" % "<version>"`,
and enable `ScalaJSPlugin` plugin.

For more details about setting up a ScalaJS project, see [here](https://www.scala-js.org/doc/project/).