import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Level._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

scalaVersion := "2.13.4"
organization := "no.uit.sfb"
organizationName := "uit-sfb"
maintainer := "mma227@uit.no"
logLevel := {
  Configurator.setAllLevels(LogManager.getRootLogger.getName, INFO)
  sbt.util.Level.Info
}

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
lazy val isRelease = settingKey[Boolean]("Is release")

useJGit

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown") }
ThisBuild / gitRefName := {
  sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value)
}
ThisBuild / isRelease := {
  sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env
    .getOrElse("CI_COMMIT_TAG", "")
    .nonEmpty
}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

ThisBuild / scalacOptions ++= Seq("-feature", "-deprecation")

enablePlugins(ScalaJSPlugin)

name := "scalajs-bindings"
scalaVersion := "2.13.4"
organization := "no.uit.sfb"
organizationName := "uit-sfb"
resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"
scalaJSUseMainModuleInitializer := false
libraryDependencies ++= Seq(
  "com.github.japgolly.scalajs-react" %%% "core" % "1.7.7"
)

gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown") }
gitRefName := {
  sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value)
}
isRelease := {
  sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env
    .getOrElse("CI_COMMIT_TAG", "")
    .nonEmpty
}
version := gitRefName.value //Do not use slug here as it would replace '.' with '-'
publishConfiguration := publishConfiguration.value.withOverwrite(true) //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
buildInfoKeys := Seq[BuildInfoKey](
  name,
  version,
  scalaVersion,
  sbtVersion,
  gitCommit
)
useCoursier := false
buildInfoPackage := s"${organization.value}.info.${name.value.toLowerCase
  .replace('-', '_')}"
//buildInfoOptions += BuildInfoOption.BuildTime
